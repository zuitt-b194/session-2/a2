
//CREATE database
CREATE DATABASE blog_db;

//CREATE "users" table
    CREATE TABLE users(
        id INT NOT NULL AUTO_INCREMENT,
        username VARCHAR(50) NOT NULL,
        password VARCHAR(300) NOT NULL,
        email VARCHAR(100),
        datetime_created DATETIME,
        PRIMARY KEY (id)
    );


//CREATE "posts" table
    CREATE TABLE posts(
        id INT NOT NULL AUTO_INCREMENT,
        author_id INT NOT NULL,
        title VARCHAR(500) NOT NULL,
        content VARCHAR(5000) NOT NULL,
        datetime_posted DATETIME,
        PRIMARY KEY (id),
        CONSTRAINT fk_posts_user_id
            FOREIGN KEY(user_id) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
    );

//CREATE "post comments" table
    CREATE TABLE post_comments(
        id INT NOT NULL AUTO_INCREMENT,
        post_id INT NOT NULL,
        user_id INT NOT NULL,
        content VARCHAR(5000) NOT NULL,
        datetime_commented DATETIME
        PRIMARY KEY(id),
        CONSTRAINT fk_post_comments_post_id
            FOREIGN KEY(post_id) REFERENCES posts(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT,
        CONSTRAINT fk_post_comments_user_id
            FOREIGN KEY(user_id) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
    );

//CREATE "post likes" table
    CREATE TABLE posts_likes(
        id INT NOT NULL AUTO_INCREMENT,
        post_id INT NOT NULL,
        user_id INT NOT NULL,
        datetime_liked DATETIME
        PRIMARY KEY(id),
        CONSTRAINT fk_post_likes_post_id
            FOREIGN KEY(post_id) REFERENCES posts(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT,
        CONSTRAINT fk_post_likes_user_id
            FOREIGN KEY(user_id) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
    );